package com.automation.qa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ProgressiveHomePage {

	WebDriver driver = null;

	public ProgressiveHomePage(WebDriver driver) {
		this.driver = driver;
	}

	By auto = By.xpath("//a[@data-tracking-label='auto_section']");
	By autoHome = By.xpath("//a[@data-tracking-label=autohome_section']");
	By autoRenter = By.xpath("//a[@data-tracking-label=autohome_section']");
	By autoCondo = By.xpath("//a[@data-tracking-label=autohome_section']");

	public void clickAuto() {
		driver.findElement(auto).click();
	}

}
