package com.automation.qa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AddDriversPOM {

	WebDriver driver;

	public AddDriversPOM(WebDriver driver) {
		this.driver = driver;
	}

	By continueToNextPage = By.xpath("//input[@text='Continue']");

	public void clickConitnue() {
		driver.findElement(continueToNextPage).click();
		;
	}

}
