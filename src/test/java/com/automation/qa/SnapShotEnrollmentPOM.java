package com.automation.qa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SnapShotEnrollmentPOM {

	WebDriver driver;
	

	public SnapShotEnrollmentPOM(WebDriver driver) {
		this.driver = driver;
	}
	
	By clickNoOnEnrollment=By.id("SnapshotFormModel_SnapshotPolicyEnrollment_Value_N");
	By continueButton = By.xpath("//input[@text='Continue']");

	
	//for next page
	By continueButton1 = By.xpath("//input[@id='next']");

	public void clickNOonSnapShotEnrolment() throws InterruptedException {
		Thread.sleep(4000);
		driver.findElement(clickNoOnEnrollment).click();
	}
	public void clickContinue() throws InterruptedException {
		driver.findElement(continueButton).click();
	}
	public void clickContinue1() throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(continueButton1).click();
	}
}
