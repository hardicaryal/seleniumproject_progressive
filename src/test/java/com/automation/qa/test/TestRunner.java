package com.automation.qa.test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.automation.qa.AddDriversPOM;
import com.automation.qa.AddVechile;
import com.automation.qa.AutoPOM;
import com.automation.qa.DriverDetails;
import com.automation.qa.PreviousInsuranceDetailPOM;
import com.automation.qa.ProgressiveHomePage;
import com.automation.qa.SnapShotEnrollmentPOM;
import com.automation.qa.StartAQuotePOM;
import com.automation.qa.report.ExtentReportConfig;


public class TestRunner {

	WebDriver driver;
	ProgressiveHomePage homePage;
	AutoPOM autoPageObj;
	StartAQuotePOM startAquoteOBJ;
	AddVechile addVehicleOBJ;
	DriverDetails driverDetailOBJ;
	AddDriversPOM driverPOM_OBJ;
	PreviousInsuranceDetailPOM prevInsuranceDetailOBJ;
	SnapShotEnrollmentPOM snapShotEnrollmentOBJ;
	ExtentReportConfig extentReportConfig;

	@BeforeTest
	public void setup() throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "/Users/hardicaryal/Downloads/chromedriver");
		driver = new ChromeDriver();
		Thread.sleep(2000);
		driver.get("https://www.progressive.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String s = driver.getTitle();
		System.out.println(s);
	}

	@Test(priority = 0)
	public void clickAutoButton() throws InterruptedException {
//		Thread.sleep(5000);
//	this.extentReportConfig.extentTest = extentReportConfig.extentReport.createTest("clickAutoButton");
		
		homePage = new ProgressiveHomePage(driver);
		Thread.sleep(2000);
		homePage.clickAuto();
	}

	@Test(priority = 1)
	public void enterZipCodeAndClick() throws InterruptedException {
		autoPageObj = new AutoPOM(driver);

		autoPageObj.enterZipCode("75062");
		autoPageObj.clickGetAQuote();
		Thread.sleep(2000);

	}

	@Test(priority = 2)
	public void startAQuote() throws InterruptedException {
		Thread.sleep(4000);
		startAquoteOBJ = new StartAQuotePOM(driver);

		startAquoteOBJ.enterFirstName("Jonny");
		startAquoteOBJ.enterLastName("Smith");
		startAquoteOBJ.enterDOB("07141991");
		startAquoteOBJ.enterAptNo("1111");
		startAquoteOBJ.enterCity("Irving");
		startAquoteOBJ.enterZipcode("75062");
		startAquoteOBJ.enterStreetName("4609 Valleyview ln");
		Thread.sleep(3000);
	
		startAquoteOBJ.clickStartAQuote();

	}

	@Test(priority = 3)
	public void addVehicle() throws InterruptedException {
		Thread.sleep(3000);
		addVehicleOBJ = new AddVechile(driver);
		addVehicleOBJ.selectVehicleYear("2015");
		addVehicleOBJ.selectBodyType("2DR 6CYL");
		addVehicleOBJ.primaryUseofVehicle("Personal (to/from work or school, errands, pleasure)");
		// addVehicleOBJ.enterZip("75062");
		addVehicleOBJ.ownOrLeaseCar("Own");
		addVehicleOBJ.lengthOfOwnership("string:B");
		addVehicleOBJ.clickAEBYes();
		addVehicleOBJ.clickBlindSpotYes();
		addVehicleOBJ.clickDone();
		addVehicleOBJ.clickContinue();
	}

	@Test(priority = 4)
	public void fillDriverDetail() throws InterruptedException {
		driverDetailOBJ = new DriverDetails(driver);
		Thread.sleep(5000);
		driverDetailOBJ.selectGenderFemale();
		driverDetailOBJ.selectGenderMale();
		driverDetailOBJ.selectMaritialStatus("string:S");
		driverDetailOBJ.selectEducation("string:7");
		driverDetailOBJ.selectEmploynment("string:EM");
		driverDetailOBJ.selectOccupation("Software Developer");
		driverDetailOBJ.enterSSN("1234567890");
		driverDetailOBJ.SelectPrimaryResedence("string:H");
		driverDetailOBJ.selectWhenYouMoved("string:N");
		driverDetailOBJ.selectYearsLicenced("string:3");
		driverDetailOBJ.clickAccidentNo();
		driverDetailOBJ.clickTicketsNo();
		driverDetailOBJ.clickContinue();
	}

	@Test(priority = 5)
	public void ConitnueFromAddDriverPage() throws InterruptedException {
		driverPOM_OBJ = new AddDriversPOM(driver);
		Thread.sleep(3000);
		driverPOM_OBJ.clickConitnue();
	}

	@Test(priority = 6)
	public void fillPrevInsuranceDetail() throws InterruptedException {
		prevInsuranceDetailOBJ = new PreviousInsuranceDetailPOM(driver);
		prevInsuranceDetailOBJ.click_No_on_having_insurance();
		prevInsuranceDetailOBJ.click_No_on_having_insurance_last_31_days();
		prevInsuranceDetailOBJ.clickNonAutoPolicyCo();
		prevInsuranceDetailOBJ.enterEmail("xt_ra@hotmail.com");
		prevInsuranceDetailOBJ.selectResidentNumber("string:3");
		prevInsuranceDetailOBJ.selectInjuryClaimNumbers("string:0");
		prevInsuranceDetailOBJ.clickContinue();
	}

	@Test(priority = 7)
	public void selectSnapShotEnrolment() throws InterruptedException {
		snapShotEnrollmentOBJ = new SnapShotEnrollmentPOM(driver);
		snapShotEnrollmentOBJ.clickNOonSnapShotEnrolment();
		snapShotEnrollmentOBJ.clickContinue();
		snapShotEnrollmentOBJ.clickContinue1();
	}
	
	@AfterTest
	public void tearDown1() {

		// driver.close();
		//driver.quit();
	}
	
}