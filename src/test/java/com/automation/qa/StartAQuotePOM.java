package com.automation.qa;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class StartAQuotePOM {

	WebDriver driver = null;

	public StartAQuotePOM(WebDriver driver) {
		this.driver = driver;
	}

	By firstName = By.xpath("//input[@name='NameAndAddressEdit_embedded_questions_list_FirstName']");
	By middleInitialas = By.xpath("//input[@name='NameAndAddressEdit_embedded_questions_list_MiddleInitial']");
	By lastName = By.xpath("//input[@name='NameAndAddressEdit_embedded_questions_list_LastName']");
	By dob = By.xpath("//input[@name='NameAndAddressEdit_embedded_questions_list_DateOfBirth']");
	//*[@id="NameAndAddressEdit_embedded_questions_list_MailingAddress"]
	By streetName = By.xpath("//input[@id='NameAndAddressEdit_embedded_questions_list_MailingAddress']");
	
	By apt = By.xpath("//input[@id='NameAndAddressEdit_embedded_questions_list_ApartmentUnit']");
	By city = By.xpath("//input[@name='NameAndAddressEdit_embedded_questions_list_City']");
	By zipCode = By.xpath("//input[@name='NameAndAddressEdit_embedded_questions_list_ZipCode']");
	By startQuoteButton = By.xpath("//button[contains(.,'Okay, start my quote.')]");

	
	

	public void enterFirstName(String fName) {
		driver.findElement(firstName).sendKeys(fName);
	}

	public void enterMiddleInitials(String middleInitials) {
		driver.findElement(middleInitialas).sendKeys(middleInitials);
	}

	public void enterLastName(String lName) {
		driver.findElement(lastName).sendKeys(lName);
	}

	public void enterDOB(String date) {
		driver.findElement(dob).sendKeys(date);
	}

	public void enterStreetName(String street) {
		driver.findElement(streetName).sendKeys(street);
		
	}

	public void enterAptNo(String aptNo) {
		driver.findElement(apt).sendKeys(aptNo);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	public void enterCity(String cityName) {
		driver.findElement(city).clear();
		driver.findElement(city).sendKeys(cityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	public void enterZipcode(String code) throws InterruptedException {
		driver.findElement(zipCode).clear();
		driver.findElement(zipCode).sendKeys(code);
		Thread.sleep(3000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	public void clickStartAQuote() {
		driver.findElement(startQuoteButton).click();
	}

}
