package com.automation.qa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AutoPOM {

	WebDriver driver = null;

	public AutoPOM(WebDriver driver) {
		this.driver = driver;
	}

	By zipCode = By.xpath("//input[@name='ZipCode']");
	By home = By.xpath("//button[@data-action='AU+HO']");
	By getAQuote = By.xpath("//input[@name='qsButton']");

	public void enterZipCode(String code) {
		driver.findElement(zipCode).sendKeys(code);
	}

	public void clickGetAQuote() {
		driver.findElement(getAQuote).click();
		;

	}

}
